package com.excursify.mobile.fragmenttabhost;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TabWidget;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    private FragmentTabHost fragmentTabHost;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        fragmentTabHost = (FragmentTabHost) view.findViewById(android.R.id.tabhost);

        fragmentTabHost.setup(getActivity(), getChildFragmentManager(), android.R.id.tabcontent);

        fragmentTabHost.addTab(fragmentTabHost.newTabSpec("fragmentb").setIndicator("Fragment B"),
                FragmentB.class, null);
        fragmentTabHost.addTab(fragmentTabHost.newTabSpec("fragmentc").setIndicator("Fragment C"),
                FragmentC.class, null);
        fragmentTabHost.addTab(fragmentTabHost.newTabSpec("fragmentd").setIndicator("Fragment D"),
                FragmentD.class, null);
        fragmentTabHost.addTab(fragmentTabHost.newTabSpec("fragmentd").setIndicator("Fragment D"),
                FragmentD.class, null);


        TabWidget tw = (TabWidget) view.findViewById(android.R.id.tabs);
        LinearLayout ll = (LinearLayout) tw.getParent();
        HorizontalScrollView hs = new HorizontalScrollView(getActivity());
        hs.setLayoutParams(new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT));
        ll.addView(hs, 0);
        ll.removeView(tw);
        hs.addView(tw);
        hs.setHorizontalScrollBarEnabled(false);



        return view;
    }

}
