package com.excursify.mobile.checkboxlistview;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    CustomAdapter dataAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        displayListView();

        checkButtonClick();
    }

    private void displayListView()
    {

        ArrayList<States> stateList = new ArrayList<States>();
        for(int i=0; i < States.states.length; i++) {
            stateList.add(States.states[i]);
        }

        //create an ArrayAdaptar from the String Array
        dataAdapter = new CustomAdapter(this,R.layout.state_info, stateList);
        ListView listView = (ListView) findViewById(R.id.listView1);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                // When clicked, show a toast with the TextView text
                States state = (States) parent.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(),"Clicked on Row: " + state.getName(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private class CustomAdapter extends ArrayAdapter<States> {

        private ArrayList<States> arrayList;

        private Context context;

        public CustomAdapter(Context context, int textViewResourceId, ArrayList<States> arrayList) {
            super(context, textViewResourceId, arrayList);
            this.arrayList = new ArrayList<States>();
            this.arrayList.addAll(arrayList);
            this.context = context;
        }

        private class ViewHolder {
            TextView code;
            CheckBox name;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;

            if(convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.state_info, null);
                viewHolder = new ViewHolder();
                viewHolder.code = (TextView) convertView.findViewById(R.id.code);
                viewHolder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
                convertView.setTag(viewHolder);

                viewHolder.name.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v;
                        States _state = (States) cb.getTag();

                        Toast.makeText(context.getApplicationContext(), "Clicked on Checkbox: " + cb.getText() + " is " + cb.isChecked(),
                                Toast.LENGTH_LONG).show();

                        _state.setSelected(cb.isChecked());

                    }
                });
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            States state = arrayList.get(position);
            viewHolder.code.setText(" (" + state.getCode() + ")");
            viewHolder.name.setText(state.getName());
            viewHolder.name.setChecked(state.isSelected());

            viewHolder.name.setTag(state);

            return convertView;


//        return super.getView(position, convertView, parent);
        }


    }

    private void checkButtonClick()
    {

        Button myButton = (Button) findViewById(R.id.findSelected);

        myButton.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                StringBuffer responseText = new StringBuffer();
                responseText.append("The following were selected...\n");

                ArrayList<States> stateList = dataAdapter.arrayList;

                for(int i=0;i<stateList.size();i++)
                {
                    States state = stateList.get(i);

                    if(state.isSelected())
                    {
                        responseText.append("\n" + state.getName());
                    }
                }

                Toast.makeText(getApplicationContext(),
                        responseText, Toast.LENGTH_LONG).show();
            }
        });
    }


}
