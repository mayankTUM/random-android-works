package com.excursify.mobile.checkboxlistview;

/**
 * Created by chaudhary on 18/01/17.
 */

public class States {
    private String name;

    private String code;

    private boolean selected;

    private States(String name, String code, boolean selected) {
        this.name = name;
        this.code = code;
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getCode() {
        return code;

    }

    public boolean isSelected() {
        return selected;
    }

    public final static States[] states = {
        new States("Delhi", "DL", false),
        new States("Punjab", "PB", true),
        new States("Hyderabad", "HY", false),
        new States("Haryana", "HR", true),
        new States("Maharashtra", "MH", false),
    };

    @Override
    public String toString() {
        return name;
    }
}
