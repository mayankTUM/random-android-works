# README #

This directory contains useful android components designed inside separate android apps. All you need to get it running is the android studio.

### What components does this repository contains? ###

* Checkboxes inside a list view.
* Datepicker and Timepicker components.
* Tab Widget + Fragment Tab Host.
* Much sleek and better advancement of the above component - Tab Layout + Page Viewer
* More components coming soon ...

### How do I get set up? ###

* Just install the latest version of android studio ([Android Studio](https://developer.android.com/studio/index.html))
* Download the components in form of separate android projects
* Open Android Studio and import the specific project
* Set up an emulator and test them out.

### Who do I talk to? ###

* mailto: mayankc3266(at)gmail.com